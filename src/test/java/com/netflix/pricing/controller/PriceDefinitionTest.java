package com.netflix.pricing.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.model.SubscriptionPlan;
import com.netflix.pricing.repository.PriceDefinitionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(PriceDefinitionController.class)
public class PriceDefinitionTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void testGetAllPriceDefinition() throws Exception {
		PriceDefinition priceDefinition1 = new PriceDefinition();
		priceDefinition1.setCountryCode("SG");
		priceDefinition1.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition1.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition1.setPrice(new BigDecimal("7.99"));

		PriceDefinition priceDefinition2 = new PriceDefinition();
		priceDefinition2.setCountryCode("PH");
		priceDefinition2.setSubscriptionPlan(SubscriptionPlan.S4);
		priceDefinition2.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition2.setPrice(new BigDecimal("9.99"));

		List<PriceDefinition> priceDefinitions = Arrays.asList(priceDefinition1, priceDefinition2);

		Mockito.when(priceDefinitionRepository.findAll()).thenReturn(priceDefinitions);

		mockMvc.perform(MockMvcRequestBuilders.get("/price_definitions/").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].countryCode", is("SG")))
				.andExpect(jsonPath("$[0].subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$[0].price", is(7.99))).andExpect(jsonPath("$[1].countryCode", is("PH")))
				.andExpect(jsonPath("$[1].subscriptionPlan", is(SubscriptionPlan.S4.toString())))
				.andExpect(jsonPath("$[1].price", is(9.99)));

	}

	@Test
	public void testCreatePriceDefinition() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/price_definitions/").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(priceDefinition)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());

		verify(priceDefinitionRepository, times(1)).save(Mockito.any(PriceDefinition.class));
		verifyNoMoreInteractions(priceDefinitionRepository);
	}
	
	@Test
	public void testCountryCodeValidationForCreate() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SGX");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/price_definitions/").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(priceDefinition)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testAmountValidationForCreate() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("-1"));

		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/price_definitions/").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(priceDefinition)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSubscriptionPlanValidationForCreate() throws Exception {
		String priceDefinitionJson = "{\"countryCode\":\"SG\", \"subscriptionPlan\":\"2S\", \"effectiveFromUtc\":\"2018-09-01T00:00:00+0000\", \"price\":\"1.99\"}";
		
		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/price_definitions/").accept(MediaType.APPLICATION_JSON)
				.content(priceDefinitionJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetPriceDefinition() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		Mockito.when(priceDefinitionRepository.findById(1L)).thenReturn(Optional.of(priceDefinition));

		mockMvc.perform(MockMvcRequestBuilders.get("/price_definitions/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(7.99)));

	}

	@Test
	public void testUpdateSubscription() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());
		Mockito.when(priceDefinitionRepository.findById(1L)).thenReturn(Optional.of(priceDefinition));

		priceDefinition.setCountryCode("TH");

		mockMvc.perform(MockMvcRequestBuilders.put("/price_definitions/1").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(priceDefinition)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.countryCode", is("TH")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(7.99)));

		verify(priceDefinitionRepository, times(1)).findById(1L);
		verify(priceDefinitionRepository, times(1)).save(Mockito.any(PriceDefinition.class));
		verifyNoMoreInteractions(priceDefinitionRepository);
	}
	
	@Test
	public void testValidationDuringUpdate() throws Exception {
		String priceDefinitionJson = "{\"countryCode\":\"SG\", \"subscriptionPlan\":\"2S\", \"effectiveFromUtc\":\"2018-09-01T00:00:00+0000\", \"price\":\"1.99\"}";
		
		Mockito.when(priceDefinitionRepository.save(Mockito.any(PriceDefinition.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.put("/price_definitions/1").accept(MediaType.APPLICATION_JSON)
				.content(priceDefinitionJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testDeleteSubscription() throws Exception {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		Mockito.when(priceDefinitionRepository.findById(1L)).thenReturn(Optional.of(priceDefinition));
		Mockito.doNothing().when(priceDefinitionRepository).delete(Mockito.any(PriceDefinition.class));

		mockMvc.perform(MockMvcRequestBuilders.delete("/price_definitions/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

		verify(priceDefinitionRepository, times(1)).findById(1L);
		verify(priceDefinitionRepository, times(1)).delete(Mockito.any(PriceDefinition.class));
		verifyNoMoreInteractions(priceDefinitionRepository);
	}

}
