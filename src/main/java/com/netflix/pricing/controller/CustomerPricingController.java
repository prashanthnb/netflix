package com.netflix.pricing.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.pricing.model.CustomerSubscriptionWithPrice;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;
import com.netflix.pricing.service.PriceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller class for APIs to get customer subscription data
 * along with the current price based on the active price definition.
 * 
 * @author nbprashanth
 */

@RestController
@RequestMapping("/customer_pricing")
@Api(description = "APIs to get Customer Subscription data along with the current price.")
public class CustomerPricingController {

	@Autowired
	private CustomerSubscriptionRepository customerSubscriptionRepository;

	@Autowired
	private PriceService priceService;

	/**
	 * Gets a list of Customer Subscriptions with the current price of the 
	 * subscription based on the Price Definitions for the given country code
	 * and subscription plan.
	 * 
	 * @return 		List of customer subscriptions with the price.
	 */
	@ApiOperation(value = "Get all Customer Subscriptions along with the current price. Use with caution for large datasets - API not paginated.")
	@GetMapping("/")
	public List<CustomerSubscriptionWithPrice> getAllSubscriptionPrice() {
		return customerSubscriptionRepository.findAll().stream()
				.map(subscription -> priceService.getCustomerSubscriptionWithPrice(subscription))
				.collect(Collectors.toList());
	}

	/**
	 * Gets a specific Customer Subscription with the current price of the
	 * subscription.
	 * 
	 * @param id	ID of the customer subscription.
	 * @return		Customer Subscription with the price.
	 */
	@ApiOperation(value = "Get a specific Customer Subscription along with the current price by the Customer ID.")
	@GetMapping("/{customerId}")
	public CustomerSubscriptionWithPrice getSubscriptionPriceByCustomerId(@PathVariable("customerId") Long customerId) {
		return priceService.getCustomerSubscriptionWithPrice(customerId);
	}
}
