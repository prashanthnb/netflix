CREATE TABLE IF NOT EXISTS `customer_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID and primary key for the customer subscription table.',
  `customer_id` int(11) NOT NULL COMMENT 'ID of the customer being referenced.',
  `country_code` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Country of subscription in ISO 2 character formet.',
  `subscription_plan` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Service plan subscribed to by the customer.',
  PRIMARY KEY (`id`),
  UNIQUE (customer_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `price_definition` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID and primary key for the price definition table.',
  `subscription_plan` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Service plan the price applies to.',
  `country_code` varchar(2) NOT NULL DEFAULT '' COMMENT 'Country of subscription.',
  `price` decimal(10,2) NOT NULL COMMENT 'Price for subscription plan.',
  `effective_from_utc` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT UC_PD UNIQUE (subscription_plan, country_code, effective_from_utc)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;