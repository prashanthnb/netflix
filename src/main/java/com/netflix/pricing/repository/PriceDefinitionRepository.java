package com.netflix.pricing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.model.SubscriptionPlan;

@Repository
public interface PriceDefinitionRepository extends JpaRepository<PriceDefinition, Long> {

	/**
	 * Finds a Price Definition record based on Country Code and Subscription Plan
	 * Ordered by Effective From Timestamp.
	 * 
	 * @param countryCode	Country Code in ISO 2 character format.
	 * @param subscriptionPlan	Subscription Plan for the Price Definition.
	 * @return	List of PriceDefinitions matching the above params.
	 */
	List<PriceDefinition> findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc(String countryCode,
			SubscriptionPlan subscriptionPlan);
}
