## Introduction
#### Purpose
Purpose of this document is to outline the implementation of the Netflix pricing service, which will host the Netflix pricing data as well as enable a systematic rollout of pricing changes across it's global customer base on a country-by-country basis. 

#### Scope
The scope of the service would be limited to the following:

-  Creating, reading, updating and deleting customer subscripting and pricing definition data.
-  Querying subscription amount for a particular customer.
-  Scheduling pricing changes by country.

## Design Overview
#### Problem Statement
Netflix rolls out price changes for our service periodically.  In order to accurately and effectively change the prices of 100M+ customers, a systematic solution for changing prices is required.

#### Technologies Used
The pricing service would be developed using the following:

-  Java 8
-  Spring framework (Spring Boot)
-  MySQL 

#### System Architecture
A high level architecture diagram is shown below.

![System Architecture Diagram](https://bitbucket.org/prashanthnb/netflix/raw/bf1da6fa36ab0ab5ac45159f265492a5eb08c212/images/Screen%20Shot%202018-09-23%20at%2010.06.31%20AM.png)

The pricing service would receive customer subscription information from an upstream service/module (say Subscription) which includes customer ID, the selected service plan and the country of subscription. The pricing service would also receive pricing definition changes, which includes service plan, country of subscription, the cost/price of subscription and the effective date. 

Given the above data, the pricing service can then be queried by the downstream services/modules (say "Payments") in order to identify the amount to be paid by a customer. The subscription amount returned would differ based on the country, subscription plan and the date/time of the request as configured.

#### Database Structure
The database consists of two tables:

-  Customer Subscription Table (customer_subscription)
    -  id
        -  Primary key for the customer subscription table.
    -  customer_id
        -  Customer ID for the subscription. Customer ID has to be unique.
    -  subscription_plan
        -  Subscription plan chosen by the cusomer (S1, S2, S4).
    -  country_code
        -  Country of subscription in ISO 2 character format.
        
-  Price Definition Table (price_definition)
    -  id
        -  Primary key for the price config table.
    -  subscription_plan
        -  Subscription plan chosen by the cusomer (S1, S2, S4).
    -  country_code
        -  Country of subscription in ISO 2 character format. Assumed to be in local currency.
    -  price
        -  Price for the subscription plan & country.
    -  effective_from_utc
        -  Datetime from which this price config is effective in local timezone.
        
The customer subscription table has a unique constraint on the customer_id field while the price definition table has a composite unique constraint on the subscription_plan, country_code and effective_from_utc fields.

Indexes can be added on price_definition.country_code, price_deinition.subscription_plan & customer_subscription.customer_id (already indexed as it is marked to be unique) to improve lookup performance. 

#### APIs

The RESTful APIs exposed by the pricing service are detailed below. More details about these APIs can be found at:

*  **Swagger UI page**: http://ec2-52-221-184-24.ap-southeast-1.compute.amazonaws.com:8080/swagger-ui.html

##### Customer Subscription API (/customer_subscriptions)

The customer subscription API is used to fetch, create, modify or delete customer subscription records. Each customer subscription record contains the customer ID, subscription plan and the country of subscription. It is assumed that the Customer details themselves are stored in a different layer and the customer ID simply refers to a customer registered with Netflix. Hence, only one record is allowed per customer ID.

##### Price Definition API (/price_definitions)

The Price Definition API is used to fetch, create, modify or delete price definition records. Each price definition contains the subscription plan, the country of subscription, the subscription price and the effective_from_utc timestamp. The effective_from_utc timestamp allows for scheduling of pricing changes and is always in UTC in order to simplify time zone related complications. 

##### Customer Pricing API (/customer_pricing)

The Customer Pricing API allows us to get the customer subscription details along with the pricing information. Essentially, it joins a customer subscription record with the active payment definition at the time of the request for the given country and subscription plan. Atleast one valid/effective payment definition is expected before this API is invoked. The API can be invoked to retreive all records (not recommended at this point for large datasets as there is no Pagination) or by customer ID (supplied during create customer subscription).

#### Business Logic & Workflow

When a customer pricing record is requested, the following steps are taken to handle the request:

1. Retreive a Customer Subscription record where the customer ID matches the given ID.
2. Identify all Price Definition records for the given country and subscription plan (taken from the customer subscription), ordered in descending order of effective from utc.
3. Identify the Price Definition with the highest timestamp before the current timestamp.
4. Create a new object with the Customer Subscription record (#1) and Price Definition (#3) and return it.

#### Performance

The goal of the pricing service is to allow for price changes seamlessly across a large scale user base. It does so by differentiating between the customer subscription data and the pricing data. Customer subscription data tends to grow with time while pricing data is independent of a customer - it only depends on the country and the subscription plan. By separating these two entities, we can ensure far less records in the pricing definition table. With proper indexing at the DB level, lookups should be fast and since pricing definitions do not change too often, caches could be used to ensure additional performance.

The pricing service also makes it easier to schedule pricing changes by allowing the user to set an effective_from_utc field, which accepts a timestamp. This ensures that a price change comes to effect from a particular point in time, which is useful while having to deal with several countries in parallel. Since pricing changes most likely carry the over head of informing the customers, having the ability to schedule the pricing changes is a practical ability to have. A wrapper API could be envisioned on top of the pricing_details API to generate pricing details for multiple countries in a single API call - if required.

### Sample Requests & Response

#### Create a Customer Subscription Record

```bash
    POST /customer_subscriptions/
    
    REQUEST BODY:
    {
	    "customerId": 1,
	    "countryCode": "SG",
	    "subscriptionPlan": "S2"
    }
    
    RESPONSE BODY:
    {
        "id": 1,
	    "customerId": 1,
	    "countryCode": "SG",
	    "subscriptionPlan": "S2"
    }
```

#### Create a Price Definition Record

```bash
    POST /price_definition/
    
    REQUEST BODY:
    {
	    "countryCode": "SG",
	    "subscriptionPlan": "S2",
        "price": "9.99",
        "effective_from_utc": "2018-09-24T11:35:00+0000"
    }
    
    RESPONSE BODY:
    {
        "id": 1,
	    "countryCode": "SG",
	    "subscriptionPlan": "S2",
        "price": "9.99",
        "effective_from_utc": "2018-09-24T11:35:00+0000"
    }
```

#### Get Customer Subscription with Price for Customer ID 

```bash
    GET /customer_pricing/1
    
    RESPONSE BODY:
    {
        "id": 1,
	    "countryCode": "SG",
	    "subscriptionPlan": "S2",
        "price": "9.99",
        "effective_from_utc": "2018-09-24T11:35:00+0000"
    }
```