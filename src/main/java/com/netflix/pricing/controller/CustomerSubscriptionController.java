package com.netflix.pricing.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.pricing.exception.CustomerSubscriptionNotFound;
import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller class for APIs to create, read, update and delete Customer 
 * Subscription records.
 * 
 * @author nbprashanth
 */
@RestController
@RequestMapping("/customer_subscriptions")
@Api(description = "Operations pertaining to fetching, creating, updating and deleting Customer Subscriptions.")
public class CustomerSubscriptionController {

	@Autowired
	private CustomerSubscriptionRepository customerSubscriptionRepository;

	/**
	 * Gets a list of Customer Subscriptions.
	 * 
	 * @return		List of Customer Subscriptions.
	 */
	@ApiOperation(value = "Get a list of all Customer Subscriptions.")
	@GetMapping("/")
	public List<CustomerSubscription> getAllSubscriptions() {
		return customerSubscriptionRepository.findAll();
	}

	/**
	 * Creates a new Customer Subscription.
	 * 
	 * @param customerSubscription	Customer Subscription to be created.
	 * @return						The Customer Subscription record created.
	 */
	@ApiOperation(value = "Create a new Customer Subscription.")
	@PostMapping("/")
	@ResponseStatus(code = HttpStatus.CREATED)
	public CustomerSubscription addSubscription(@Valid @RequestBody CustomerSubscription customerSubscription) {
		return customerSubscriptionRepository.save(customerSubscription);
	}

	/**
	 * Gets a Customer Subscription by ID.
	 * 
	 * @param id	ID of the Customer Subscription to retrieve.
	 * @return		Customer Subscription record.
	 */
	@ApiOperation(value = "Get a specific Customer Subscription by ID.")
	@GetMapping("/{id}")
	public CustomerSubscription getSubscriptionById(@PathVariable("id") Long id) {
		return customerSubscriptionRepository.findById(id).orElseThrow(
				() -> new CustomerSubscriptionNotFound("Unable to get Customer Subscription for id: " + id));
	}

	/**
	 * Updates a Customer Subscription record.
	 * 
	 * @param id	ID of the customer subscription to update.
	 * @param newCustomerSubscription	Updated Customer Subscription record.
	 * @return		Updated Customer Subscription.
	 */
	@ApiOperation(value = "Update a specific Customer Subscription by ID.")
	@PutMapping("/{id}")
	public CustomerSubscription updateSubscription(@PathVariable("id") Long id,
			@Valid @RequestBody CustomerSubscription newCustomerSubscription) {
		CustomerSubscription customerSubscription = customerSubscriptionRepository.findById(id).orElseThrow(
				() -> new CustomerSubscriptionNotFound("Unable to get Customer Subscription for id: " + id));

		customerSubscription.setCountryCode(newCustomerSubscription.getCountryCode());
		customerSubscription.setCustomerId(newCustomerSubscription.getCustomerId());
		customerSubscription.setSubscriptionPlan(newCustomerSubscription.getSubscriptionPlan());

		return customerSubscriptionRepository.save(customerSubscription);
	}

	/**
	 * Deletes a Customer Subscription record.
	 * 
	 * @param id	ID of the Customer Subscription record.
	 */
	@ApiOperation(value = "Delete a specific Customer Subscription by ID.")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteSubscription(@PathVariable("id") Long id) {
		CustomerSubscription customerSubscription = customerSubscriptionRepository.findById(id).orElseThrow(
				() -> new CustomerSubscriptionNotFound("Unable to get Customer Subscription for id: " + id));
		customerSubscriptionRepository.delete(customerSubscription);

		return ResponseEntity.ok().build();
	}

}
