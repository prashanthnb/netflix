package com.netflix.pricing.model;

public enum SubscriptionPlan {

	S1("1S"), S2("2S"), S4("4S");

	private final String subscriptionPlanCode;

	SubscriptionPlan(String subscriptionPlanCode) {
		this.subscriptionPlanCode = subscriptionPlanCode;
	}

	public String getSubscriptionPlanCode() {
		return subscriptionPlanCode;
	}
}
