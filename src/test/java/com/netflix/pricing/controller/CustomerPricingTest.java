package com.netflix.pricing.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.model.SubscriptionPlan;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;
import com.netflix.pricing.repository.PriceDefinitionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerPricingController.class)
@ComponentScan(basePackages = "com.netflix.pricing.impl")
public class CustomerPricingTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerSubscriptionRepository customerSubscriptionRepository;

	@MockBean
	private PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	ObjectMapper objectMapper;
	
	@Test
	public void testGetAllSubscriptions() throws Exception {
		CustomerSubscription customerSubscription1 = new CustomerSubscription();
		customerSubscription1.setCustomerId(10L);
		customerSubscription1.setCountryCode("SG");
		customerSubscription1.setSubscriptionPlan(SubscriptionPlan.S2);

		CustomerSubscription customerSubscription2 = new CustomerSubscription();
		customerSubscription2.setCustomerId(11L);
		customerSubscription2.setCountryCode("PH");
		customerSubscription2.setSubscriptionPlan(SubscriptionPlan.S4);

		when(customerSubscriptionRepository.findAll())
				.thenReturn(Arrays.asList(customerSubscription1, customerSubscription2));

		PriceDefinition priceDefinition1 = new PriceDefinition();
		priceDefinition1.setCountryCode("SG");
		priceDefinition1.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition1.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition1.setPrice(new BigDecimal("7.99"));

		PriceDefinition priceDefinition2 = new PriceDefinition();
		priceDefinition2.setCountryCode("PH");
		priceDefinition2.setSubscriptionPlan(SubscriptionPlan.S4);
		priceDefinition2.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition2.setPrice(new BigDecimal("5.99"));

		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition1));
		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("PH",
				SubscriptionPlan.S4)).thenReturn(Arrays.asList(priceDefinition2));

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_pricing/").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].customerId", is(10)))
				.andExpect(jsonPath("$[0].countryCode", is("SG")))
				.andExpect(jsonPath("$[0].subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$[0].price", is(7.99))).andExpect(jsonPath("$[1].customerId", is(11)))
				.andExpect(jsonPath("$[1].countryCode", is("PH")))
				.andExpect(jsonPath("$[1].subscriptionPlan", is(SubscriptionPlan.S4.toString())))
				.andExpect(jsonPath("$[1].price", is(5.99)));
	}

	@Test
	public void testGetSubscriptionByCustomerId() throws Exception {
		CustomerSubscription customerSubscription1 = new CustomerSubscription();
		customerSubscription1.setCustomerId(10L);
		customerSubscription1.setCountryCode("SG");
		customerSubscription1.setSubscriptionPlan(SubscriptionPlan.S2);

		when(customerSubscriptionRepository.findByCustomerId(10L)).thenReturn(Optional.of(customerSubscription1));

		PriceDefinition priceDefinition1 = new PriceDefinition();
		priceDefinition1.setCountryCode("SG");
		priceDefinition1.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition1.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis()));
		priceDefinition1.setPrice(new BigDecimal("7.99"));

		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition1));

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_pricing/10").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.customerId", is(10))).andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(7.99)));
	}
	
	@Test
	public void testGetSubscriptionByCustomerIdMultipleDefinitions() throws Exception {
		CustomerSubscription customerSubscription1 = new CustomerSubscription();
		customerSubscription1.setCustomerId(10L);
		customerSubscription1.setCountryCode("SG");
		customerSubscription1.setSubscriptionPlan(SubscriptionPlan.S2);

		when(customerSubscriptionRepository.findByCustomerId(10L)).thenReturn(Optional.of(customerSubscription1));

		PriceDefinition priceDefinition1 = new PriceDefinition();
		priceDefinition1.setCountryCode("SG");
		priceDefinition1.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition1.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis() - 1000 * 30));
		priceDefinition1.setPrice(new BigDecimal("7.99"));
		
		PriceDefinition priceDefinition2 = new PriceDefinition();
		priceDefinition2.setCountryCode("SG");
		priceDefinition2.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition2.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis() - 1000 * 10));
		priceDefinition2.setPrice(new BigDecimal("8.99"));

		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition1, priceDefinition2));

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_pricing/10").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.customerId", is(10))).andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(8.99)));
	}
	
	@Test
	public void testGetSubscriptionByCustomerIdPriceChange() throws Exception {
		CustomerSubscription customerSubscription1 = new CustomerSubscription();
		customerSubscription1.setCustomerId(10L);
		customerSubscription1.setCountryCode("SG");
		customerSubscription1.setSubscriptionPlan(SubscriptionPlan.S2);

		when(customerSubscriptionRepository.findByCustomerId(10L)).thenReturn(Optional.of(customerSubscription1));

		long effectiveTimeMillis = System.currentTimeMillis() - 10000;
		long effectiveTimeMillisForChange = System.currentTimeMillis() + 10000;
		
		PriceDefinition priceDefinition1 = new PriceDefinition();
		priceDefinition1.setCountryCode("SG");
		priceDefinition1.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition1.setEffectiveFromUtc(new Timestamp(effectiveTimeMillis));
		priceDefinition1.setPrice(new BigDecimal("7.99"));
		
		PriceDefinition priceDefinition2 = new PriceDefinition();
		priceDefinition2.setCountryCode("SG");
		priceDefinition2.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition2.setEffectiveFromUtc(new Timestamp(effectiveTimeMillisForChange));
		priceDefinition2.setPrice(new BigDecimal("8.99"));

		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition1, priceDefinition2));

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_pricing/10").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.customerId", is(10))).andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(7.99)));
		
		// TODO: Find a better way to simulate this.
		while(System.currentTimeMillis() < effectiveTimeMillisForChange) {
			Thread.sleep(1000);
		}
		
		mockMvc.perform(MockMvcRequestBuilders.get("/customer_pricing/10").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.customerId", is(10))).andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$.price", is(8.99)));
	}

}
