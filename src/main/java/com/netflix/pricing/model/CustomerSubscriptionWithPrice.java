package com.netflix.pricing.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class CustomerSubscriptionWithPrice {

	private Long customerId;
	private String countryCode;
	private SubscriptionPlan subscriptionPlan;
	private BigDecimal price;
	private Timestamp effective_from_utc;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public SubscriptionPlan getSubscriptionPlan() {
		return subscriptionPlan;
	}

	public void setSubscriptionPlan(SubscriptionPlan subscriptionPlan) {
		this.subscriptionPlan = subscriptionPlan;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Timestamp getEffective_from_utc() {
		return effective_from_utc;
	}

	public void setEffective_from_utc(Timestamp effective_from_utc) {
		this.effective_from_utc = effective_from_utc;
	}

	@Override
	public String toString() {
		return "CustomerSubscriptionWithPrice [customerId=" + customerId + ", countryCode=" + countryCode
				+ ", subscriptionPlan=" + subscriptionPlan + ", price=" + price + ", effective_from_utc="
				+ effective_from_utc + "]";
	}

}
