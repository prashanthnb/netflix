package com.netflix.pricing.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.model.CustomerSubscriptionWithPrice;
import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.model.SubscriptionPlan;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;
import com.netflix.pricing.repository.PriceDefinitionRepository;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PriceServiceTest {

	@Autowired
	private PriceService priceService;

	@MockBean
	private PriceDefinitionRepository priceDefinitionRepository;

	@MockBean
	private CustomerSubscriptionRepository CustomerSubscriptionRepository;
	
	@Test
	public void testGetCustomerSubscriptionWithPrice() {
		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis() - 1000 * 10));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);
		
		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition));

		CustomerSubscriptionWithPrice customerSubscriptionWithPrice = priceService
				.getCustomerSubscriptionWithPrice(customerSubscription);

		assertEquals(Long.valueOf(10), customerSubscriptionWithPrice.getCustomerId());
		assertEquals("SG", customerSubscriptionWithPrice.getCountryCode());
		assertEquals(SubscriptionPlan.S2, customerSubscriptionWithPrice.getSubscriptionPlan());
		assertEquals(new BigDecimal("7.99"), customerSubscriptionWithPrice.getPrice());

	}

	@Test
	public void testGetCustomerSubscriptionWithPriceById() {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		when(CustomerSubscriptionRepository.findByCustomerId(10L)).thenReturn(Optional.of(customerSubscription));

		PriceDefinition priceDefinition = new PriceDefinition();
		priceDefinition.setCountryCode("SG");
		priceDefinition.setSubscriptionPlan(SubscriptionPlan.S2);
		priceDefinition.setEffectiveFromUtc(new Timestamp(System.currentTimeMillis() - 10000));
		priceDefinition.setPrice(new BigDecimal("7.99"));

		when(priceDefinitionRepository.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc("SG",
				SubscriptionPlan.S2)).thenReturn(Arrays.asList(priceDefinition));

		CustomerSubscriptionWithPrice customerSubscriptionWithPrice = priceService
				.getCustomerSubscriptionWithPrice(10L);

		assertEquals(Long.valueOf(10), customerSubscriptionWithPrice.getCustomerId());
		assertEquals("SG", customerSubscriptionWithPrice.getCountryCode());
		assertEquals(SubscriptionPlan.S2, customerSubscriptionWithPrice.getSubscriptionPlan());
		assertEquals(new BigDecimal("7.99"), customerSubscriptionWithPrice.getPrice());

	}

}
