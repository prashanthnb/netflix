package com.netflix.pricing.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.pricing.exception.PriceDefinitionNotFound;
import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.repository.PriceDefinitionRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Controller class for APIs to create, read, update and delete Price 
 * Definitions records.
 * 
 * @author nbprashanth
 */
@RestController
@RequestMapping("/price_definitions")
@Api(description = "Operations pertaining to fetching, creating, updating and deleting Price Definitions.")
public class PriceDefinitionController {

	@Autowired
	private PriceDefinitionRepository priceDefinitionRepository;

	/**
	 * Gets a list of Price Definitions.
	 * 
	 * @return	List of Price Definitions.
	 */
	@ApiOperation(value = "Get a list of all Price Definitions.")
	@GetMapping("/")
	public List<PriceDefinition> getAllDefinitions() {
		return priceDefinitionRepository.findAll();
	}

	/**
	 * Created a new Price Definition.
	 * 
	 * @param priceDefinition	Price Definition to create.
	 * @return					Created Price Definition.
	 */
	@ApiOperation(value = "Create a new Price Definition.")
	@PostMapping("/")
	@ResponseStatus(code = HttpStatus.CREATED)
	public PriceDefinition addDefinition(@Valid @RequestBody PriceDefinition priceDefinition) {
		return priceDefinitionRepository.save(priceDefinition);
	}

	/**
	 * Gets a Price Definition by ID.
	 * 
	 * @param id	ID of the Price Definition to fetch.
	 * @return		Price Definition corresponding to the ID.
	 */
	@ApiOperation(value = "Get a specific Price Definition by ID.")
	@GetMapping("/{id}")
	public PriceDefinition getDefinitionById(@PathVariable("id") Long id) {
		return priceDefinitionRepository.findById(id)
				.orElseThrow(() -> new PriceDefinitionNotFound("Unable to get Price Definition for id: " + id));
	}

	/**
	 * Updates a Price Definition.
	 * 
	 * @param id	ID of the Price Definition to update.
	 * @param newPriceDefinition		Updated Price Definition record.
	 * @return		Updates Price Definition record.
	 */
	@ApiOperation(value = "Update a specific Price Definition by its ID.")
	@PutMapping("/{id}")
	public PriceDefinition updateDefinition(@PathVariable("id") Long id,
			@Valid @RequestBody PriceDefinition newPriceDefinition) {
		PriceDefinition priceDefinition = priceDefinitionRepository.findById(id)
				.orElseThrow(() -> new PriceDefinitionNotFound("Unable to get Price Definition for id: " + id));

		priceDefinition.setCountryCode(newPriceDefinition.getCountryCode());
		priceDefinition.setSubscriptionPlan(newPriceDefinition.getSubscriptionPlan());
		priceDefinition.setEffectiveFromUtc(newPriceDefinition.getEffectiveFromUtc());
		priceDefinition.setPrice(newPriceDefinition.getPrice());

		return priceDefinitionRepository.save(priceDefinition);
	}

	/**
	 * Deletes a Price Definition.
	 * 
	 * @param id	ID of the Price Definition to delete.
	 */
	@ApiOperation(value = "Delete a Price Definition by its ID.")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteDefinition(@PathVariable("id") Long id) {
		PriceDefinition priceDefinition = priceDefinitionRepository.findById(id)
				.orElseThrow(() -> new PriceDefinitionNotFound("Unable to get Price Definition for id: " + id));
		priceDefinitionRepository.delete(priceDefinition);

		return ResponseEntity.ok().build();
	}

}
