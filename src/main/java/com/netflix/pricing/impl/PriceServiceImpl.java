package com.netflix.pricing.impl;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.pricing.exception.CustomerSubscriptionNotFound;
import com.netflix.pricing.exception.PriceDefinitionNotFound;
import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.model.CustomerSubscriptionWithPrice;
import com.netflix.pricing.model.PriceDefinition;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;
import com.netflix.pricing.repository.PriceDefinitionRepository;
import com.netflix.pricing.service.PriceService;

/**
 * Service to convert Customer Subscription ID or Customer Subscription record
 * to a Customer Subscription with Price record by adding price and effective from 
 * UTC time data.
 * 
 * @author nbprashanth
 */
@Service
public class PriceServiceImpl implements PriceService {

	@Autowired
	private CustomerSubscriptionRepository customerSubscriptionRepository;

	@Autowired
	private PriceDefinitionRepository priceDefinitionRepository;

	/**
	 * Converts CustomerSubscription record to CustomerSubscriptionWithPrice based on
	 * Customer Subscription ID.
	 * 
	 * @return CustomerSubscription record with price & effective from timestamp.
	 */
	@Override
	public CustomerSubscriptionWithPrice getCustomerSubscriptionWithPrice(Long customerId) {
		CustomerSubscription customerSubscription = customerSubscriptionRepository.findByCustomerId(customerId)
				.orElseThrow(() -> new CustomerSubscriptionNotFound(
						"Unable to find Customer Subscription record for customer ID: " + customerId));

		return getCustomerSubscriptionWithPrice(customerSubscription);
	}

	/**
	 * Converts CustomerSubscription record to CustomerSubscriptionWithPrice based on
	 * Customer Subscription record.
	 * 
	 * @return CustomerSubscription record with price & effective from timestamp.
	 */
	@Override
	public CustomerSubscriptionWithPrice getCustomerSubscriptionWithPrice(CustomerSubscription customerSubscription) {
		List<PriceDefinition> priceDefinitions = priceDefinitionRepository
				.findByCountryCodeAndSubscriptionPlanOrderByEffectiveFromUtcDesc(customerSubscription.getCountryCode(),
						customerSubscription.getSubscriptionPlan());

		PriceDefinition priceDefinition = priceDefinitions.stream()
				.filter(pd -> pd.getEffectiveFromUtc().before(new Timestamp(System.currentTimeMillis())))
				.max((x, y) -> x.getEffectiveFromUtc().compareTo(y.getEffectiveFromUtc()))
				.orElseThrow(() -> new PriceDefinitionNotFound(
						"Unable to get Price Definition for country: " + customerSubscription.getCountryCode()
								+ " and subscription plan: " + customerSubscription.getSubscriptionPlan()));

		CustomerSubscriptionWithPrice customerSubscriptionWithPrice = new CustomerSubscriptionWithPrice();
		customerSubscriptionWithPrice.setCustomerId(customerSubscription.getCustomerId());
		customerSubscriptionWithPrice.setCountryCode(customerSubscription.getCountryCode());
		customerSubscriptionWithPrice.setSubscriptionPlan(customerSubscription.getSubscriptionPlan());
		customerSubscriptionWithPrice.setPrice(priceDefinition.getPrice());
		customerSubscriptionWithPrice.setEffective_from_utc(priceDefinition.getEffectiveFromUtc());

		return customerSubscriptionWithPrice;
	}

	public CustomerSubscriptionRepository getCustomerSubscriptionRepository() {
		return customerSubscriptionRepository;
	}

	public void setCustomerSubscriptionRepository(CustomerSubscriptionRepository customerSubscriptionRepository) {
		this.customerSubscriptionRepository = customerSubscriptionRepository;
	}

	public PriceDefinitionRepository getPriceDefinitionRepository() {
		return priceDefinitionRepository;
	}

	public void setPriceDefinitionRepository(PriceDefinitionRepository priceDefinitionRepository) {
		this.priceDefinitionRepository = priceDefinitionRepository;
	}

}
