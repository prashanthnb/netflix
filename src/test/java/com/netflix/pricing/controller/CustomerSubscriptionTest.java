package com.netflix.pricing.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.model.SubscriptionPlan;
import com.netflix.pricing.repository.CustomerSubscriptionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerSubscriptionController.class)
public class CustomerSubscriptionTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerSubscriptionRepository customerSubscriptionRepository;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void testGetAllCustomerSubscription() throws Exception {
		CustomerSubscription customerSubscription1 = new CustomerSubscription();
		customerSubscription1.setCustomerId(10L);
		customerSubscription1.setCountryCode("SG");
		customerSubscription1.setSubscriptionPlan(SubscriptionPlan.S2);

		CustomerSubscription customerSubscription2 = new CustomerSubscription();
		customerSubscription2.setCustomerId(11L);
		customerSubscription2.setCountryCode("PH");
		customerSubscription2.setSubscriptionPlan(SubscriptionPlan.S4);

		List<CustomerSubscription> customerSubscriptions = Arrays.asList(customerSubscription1, customerSubscription2);

		Mockito.when(customerSubscriptionRepository.findAll()).thenReturn(customerSubscriptions);

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_subscriptions/").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].customerId", is(10)))
				.andExpect(jsonPath("$[0].countryCode", is("SG")))
				.andExpect(jsonPath("$[0].subscriptionPlan", is(SubscriptionPlan.S2.toString())))
				.andExpect(jsonPath("$[1].customerId", is(11))).andExpect(jsonPath("$[1].countryCode", is("PH")))
				.andExpect(jsonPath("$[1].subscriptionPlan", is(SubscriptionPlan.S4.toString())));

	}

	@Test
	public void testCreateCustomerSubscription() throws Exception {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		Mockito.when(customerSubscriptionRepository.save(Mockito.any(CustomerSubscription.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/customer_subscriptions/").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(customerSubscription)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());

		verify(customerSubscriptionRepository, times(1)).save(Mockito.any(CustomerSubscription.class));
		verifyNoMoreInteractions(customerSubscriptionRepository);
	}
	
	@Test
	public void testCountryCodeValidation() throws Exception {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SGX");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		Mockito.when(customerSubscriptionRepository.save(Mockito.any(CustomerSubscription.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/customer_subscriptions/").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(customerSubscription)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSubscriptionPlanValidation() throws Exception {
		String customerSubscriptionJson = "{\"customerId\":\"10\", \"countryCode\":\"SG\", \"subscriptionPlan\":\"1S\"}";

		Mockito.when(customerSubscriptionRepository.save(Mockito.any(CustomerSubscription.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.post("/customer_subscriptions/").accept(MediaType.APPLICATION_JSON)
				.content(customerSubscriptionJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetCustomerSubscription() throws Exception {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		Mockito.when(customerSubscriptionRepository.findById(1L)).thenReturn(Optional.of(customerSubscription));

		mockMvc.perform(MockMvcRequestBuilders.get("/customer_subscriptions/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.customerId", is(10))).andExpect(jsonPath("$.countryCode", is("SG")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())));

	}

	@Test
	public void testUpdateSubscription() throws Exception {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		Mockito.when(customerSubscriptionRepository.save(Mockito.any(CustomerSubscription.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());
		Mockito.when(customerSubscriptionRepository.findById(1L)).thenReturn(Optional.of(customerSubscription));

		customerSubscription.setCountryCode("TH");

		mockMvc.perform(MockMvcRequestBuilders.put("/customer_subscriptions/1").accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsBytes(customerSubscription)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.customerId", is(10)))
				.andExpect(jsonPath("$.countryCode", is("TH")))
				.andExpect(jsonPath("$.subscriptionPlan", is(SubscriptionPlan.S2.toString())));

		verify(customerSubscriptionRepository, times(1)).findById(1L);
		verify(customerSubscriptionRepository, times(1)).save(Mockito.any(CustomerSubscription.class));
		verifyNoMoreInteractions(customerSubscriptionRepository);
	}
	
	@Test
	public void testValidationDuringUpdate() throws Exception {
		String customerSubscriptionJson = "{\"customerId\":\"10\", \"countryCode\":\"SG\", \"subscriptionPlan\":\"1S\"}";

		Mockito.when(customerSubscriptionRepository.save(Mockito.any(CustomerSubscription.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		mockMvc.perform(MockMvcRequestBuilders.put("/customer_subscriptions/1").accept(MediaType.APPLICATION_JSON)
				.content(customerSubscriptionJson).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testDeleteSubscription() throws Exception {
		CustomerSubscription customerSubscription = new CustomerSubscription();
		customerSubscription.setCustomerId(10L);
		customerSubscription.setCountryCode("SG");
		customerSubscription.setSubscriptionPlan(SubscriptionPlan.S2);

		Mockito.when(customerSubscriptionRepository.findById(1L)).thenReturn(Optional.of(customerSubscription));
		Mockito.doNothing().when(customerSubscriptionRepository).delete(Mockito.any(CustomerSubscription.class));

		mockMvc.perform(MockMvcRequestBuilders.delete("/customer_subscriptions/1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

		verify(customerSubscriptionRepository, times(1)).findById(1L);
		verify(customerSubscriptionRepository, times(1)).delete(Mockito.any(CustomerSubscription.class));
		verifyNoMoreInteractions(customerSubscriptionRepository);
	}

}
