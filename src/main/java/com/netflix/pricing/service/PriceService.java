package com.netflix.pricing.service;

import com.netflix.pricing.model.CustomerSubscription;
import com.netflix.pricing.model.CustomerSubscriptionWithPrice;

public interface PriceService {

	public CustomerSubscriptionWithPrice getCustomerSubscriptionWithPrice(Long customerId);

	public CustomerSubscriptionWithPrice getCustomerSubscriptionWithPrice(CustomerSubscription customerSubscription);

}
