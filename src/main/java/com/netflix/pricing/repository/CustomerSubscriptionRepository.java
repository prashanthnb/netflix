package com.netflix.pricing.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netflix.pricing.model.CustomerSubscription;

@Repository
public interface CustomerSubscriptionRepository extends JpaRepository<CustomerSubscription, Long> {

	/** 
	 * Finds Customer Subscription record based on Customer ID.
	 * 
	 * @param customerId		Customer ID related to the customer subscription.
	 * @return				CustomerSubscription record for the given Customer ID, if any.
	 */
	Optional<CustomerSubscription> findByCustomerId(Long customerId);

}
